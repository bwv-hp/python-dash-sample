import re

_format_convertor = (
    ('yyyy', '%Y'), ('yyy', '%Y'), ('yy', '%y'), ('y', '%y'),
    ('MMMM', '%B'), ('MMM', '%b'), ('MM', '%m'), ('M', '%m'),
    ('dddd', '%A'), ('ddd', '%a'), ('dd', '%d'), ('d', '%d'),
    ('HH', '%H'), ('H', '%H'), ('hh', '%I'), ('h', '%I'),
    ('mm', '%M'), ('m', '%M'),
    ('ss', '%S'), ('s', '%S'),
    ('tt', '%p'), ('t', '%p'),
    ('fff', '%f'),
    ('zzz', '%z'), ('zz', '%z'), ('z', '%z'),
)


def convert_py_datetime_format(in_format):
    out_format = ''
    while in_format:
        if in_format[0] == "'":
            apos = in_format.find("'", 1)
            if apos == -1:
                apos = len(in_format)
            out_format += in_format[1:apos].replace('%', '%%')
            in_format = in_format[apos + 1:]
        elif in_format[0] == '\\':
            out_format += in_format[1:2].replace('%', '%%')
            in_format = in_format[2:]
        else:
            for intok, outtok in _format_convertor:
                if in_format.startswith(intok):
                    out_format += outtok
                    in_format = in_format[len(intok):]
                    break
            else:
                out_format += in_format[0].replace('%', '%%')
                in_format = in_format[1:]
    return out_format


def extract_csv_col_config(col_name: str):
    try:
        found = re.search('\\((.*)\\)', col_name)
        col_name = col_name.replace(found.group(0), '')
        config_string = found.group(1)
        config_list = config_string.split(';')
        configs = []
        for config in config_list:
            key_value_list = config.split('=')
            key = key_value_list[0]
            value = key_value_list[1] if len(key_value_list) > 1 else None
            configs.append((key, value))
    except AttributeError:
        configs = []
    return col_name, configs
