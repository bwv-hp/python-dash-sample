import dash
import pandas as pd
import plotly.express as px
from dash import dcc
from dash import html

app = dash.Dash(__name__)

df = pd.read_csv('csv/graph_sample.csv')
print(df)
df['DateTime'] = pd.to_datetime(df['DateTime'], format='%Y%m%d %H:%M:%S.%f')

line_fig = px.line(df, x='DateTime', y=['DATA 1', 'DATA 2', 'DATA 3', 'DATA 4'])

app.layout = html.Div(children=[
    dcc.Graph(id='graph', figure=line_fig)
])

if __name__ == '__main__':
    app.run_server(debug=True)
