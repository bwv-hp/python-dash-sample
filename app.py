from datetime import datetime, timedelta

import dash
import numpy as np
import pandas as pd
import plotly.express as px
from dash import dcc, Output, Input
from dash import html

from utils import extract_csv_col_config, convert_py_datetime_format


def process_csv_variable(df_param):
    # process x-axis csv variable
    old_x_col_name = df_param.columns[0]
    new_x_col_name, configs = extract_csv_col_config(old_x_col_name)
    datetime_format = configs[0][0]
    df_param = df_param.rename(columns={old_x_col_name: new_x_col_name})
    df_param[new_x_col_name] = pd.to_datetime(df_param[new_x_col_name],
                                              format=convert_py_datetime_format(datetime_format))
    # process y-axis csv variable
    y_col_name_list = df_param.columns[1:]
    for old_y_col_name in y_col_name_list:
        new_y_col_name, configs = extract_csv_col_config(old_y_col_name)
        df_param = df_param.rename(columns={old_y_col_name: new_y_col_name})
        for config, value in configs:
            if config == 'minFilter':
                df_param.loc[df_param[new_y_col_name] < int(value), new_y_col_name] = np.nan
            elif config == 'maxFilter':
                df_param.loc[df_param[new_y_col_name] > int(value), new_y_col_name] = np.nan
    return df_param


app = dash.Dash(__name__)

app.layout = html.Div(id='container', children=[
    dcc.DatePickerRange(
        id='date-picker-range',
        minimum_nights=0,
        display_format='YYYY/MM/DD'
    ),
    dcc.Graph(id='scatter-graph'),
])


@app.callback(
    Output('date-picker-range', 'start_date'),
    Output('date-picker-range', 'end_date'),
    Input('container', 'id')
)
def update_date_picker(id):
    df = pd.read_csv('csv/app_sample.csv')
    df = process_csv_variable(df)
    x_col_name = df.columns[0]

    init_start_date = df[x_col_name].min().strftime('%Y-%m-%d')
    init_end_date = df[x_col_name].max().strftime('%Y-%m-%d')
    return init_start_date, init_end_date


@app.callback(
    Output('scatter-graph', 'figure'),
    Input('date-picker-range', 'start_date'),
    Input('date-picker-range', 'end_date')
)
def update_figure(start_date, end_date):
    df = pd.read_csv('csv/app_sample.csv')
    df = process_csv_variable(df)
    if start_date is not None and end_date is not None:
        start_date = datetime.fromisoformat(start_date)
        end_date = datetime.fromisoformat(end_date) + timedelta(days=1)
        # get first columns name for x-axis
        x_col_name = df.columns[0]
        # get list column name except first column for y-axis
        y_col_name_list = df.columns[1:]
        filtered_df = df[(start_date <= df[x_col_name]) & (df[x_col_name] <= end_date)]
        scatter_fig = px.scatter(filtered_df, x=x_col_name, y=y_col_name_list)

        return scatter_fig


if __name__ == '__main__':
    app.run_server(debug=True)
