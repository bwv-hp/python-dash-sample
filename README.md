# python-dash-sample

## Required packages
Install required Python packages
```bash
pip install -r requirements.txt
```
## Examples
First, in the terminal run one of the command lines corresponding to the examples:
- HTML example: `python html_demo.py`
- Graph example: `python graph_demo.py`
- Callbacks example: `python callbacks_demo.py`
- Optimizing and adding functionality example: `python app.py`

In the browser go to http://127.0.0.1:8050/ to see the result.
