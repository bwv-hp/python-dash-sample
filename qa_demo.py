from datetime import datetime, timedelta

import dash
import numpy as np
import pandas as pd
import plotly.express as px
from dash import dcc, Output, Input
from dash import html

app = dash.Dash(__name__)

df = pd.read_csv('csv/callbacks_sample.csv')
df['DateTime'] = pd.to_datetime(df['DateTime'], format='%Y%m%d %H:%M:%S.%f')

init_start_date = df['DateTime'].min().strftime('%Y-%m-%d')
init_end_date = df['DateTime'].max().strftime('%Y-%m-%d')

app.layout = html.Div(children=[
    dcc.DatePickerRange(
        id='date-picker-range',
        start_date=init_start_date,
        end_date=init_end_date,
        minimum_nights=0,
        display_format='YYYY/MM/DD'
    ),
    html.Div(
        children=[
            dcc.Input(id="min-y", type="number", placeholder="Range Y min"),
            dcc.Input(id="max-y", type="number", placeholder="Range Y max"),
        ],
        style={'marginTop': '20px'}
    ),
    html.Div(
        children=[
            dcc.Checklist(
                id='low-pass-filter',
                options=[
                    {'label': 'Low pass filter', 'value': 'LPF'},
                ],
            ),
        ],
        style={'marginTop': '20px'}
    ),
    dcc.Graph(id='scatter-graph'),
])


@app.callback(
    Output('scatter-graph', 'figure'),
    Input('date-picker-range', 'start_date'),
    Input('date-picker-range', 'end_date'),
    Input('min-y', 'value'),
    Input('max-y', 'value'),
    Input('low-pass-filter', 'value')
)
def update_figure(start_date, end_date, range_y_min, range_y_max, lpf):
    if start_date is not None and end_date is not None:
        start_date = datetime.fromisoformat(start_date)
        end_date = datetime.fromisoformat(end_date) + timedelta(days=1)
        filtered_df = df[(start_date <= df['DateTime']) & (df['DateTime'] <= end_date)]
        # for demo purposes, it's just filtering out data greater than 100, not LPF
        if lpf is not None and len(lpf) > 0:
            y_col_name_list = filtered_df.columns[1:]
            for y_col_name in y_col_name_list:
                filtered_df.loc[filtered_df[y_col_name] > 100, y_col_name] = np.nan
        scatter_fig = px.scatter(filtered_df, x='DateTime', y=['DATA 1', 'DATA 2', 'DATA 3', 'DATA 4'],
                                 range_x=[start_date, end_date], range_y=[range_y_min, range_y_max])

        return scatter_fig


if __name__ == '__main__':
    app.run_server(debug=True)
